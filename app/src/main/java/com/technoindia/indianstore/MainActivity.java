package com.technoindia.indianstore;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.navigation.NavigationView;
import com.technoindia.indianstore.Adapter.ProductCategoryAdapter;
import com.google.android.material.tabs.TabLayout;
import com.technoindia.indianstore.Adapter.SliderAdapter;
import com.technoindia.indianstore.Api.ApiClient;
import com.technoindia.indianstore.Api.ApiInterface;
import com.technoindia.indianstore.Model.Categories;
import com.technoindia.indianstore.Model.ProductCategory;
import com.technoindia.indianstore.Utils.FullScreenLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class  MainActivity extends AppCompatActivity {
    RecyclerView productCategoryRecyclerView;
    ArrayList<Categories> productCategoryModelArrayList;
    ProductCategoryAdapter productCategoryAdapter;
    FullScreenLoader fullScreenLoader;
    DrawerLayout drawer;
    NavigationView navigationView;
    TextView appName;

    List<Integer> icon;// List<String> iconName;


    ViewPager viewPager;
    TabLayout indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        appName = findViewById(R.id.app_name);
        fullScreenLoader = new FullScreenLoader(this);
        productCategoryRecyclerView = findViewById(R.id.product_category);
        GridLayoutManager manager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        productCategoryRecyclerView.setLayoutManager(manager);
        getProductCategory();

        String missionTxt = "<font color=#f78930>Indian</font> <font color=#22b04d>Store</font>";
        appName.setText(Html.fromHtml(missionTxt), TextView.BufferType.SPANNABLE);

        viewPager = findViewById(R.id.viewPager);
        indicator = findViewById(R.id.indicator);
        icon = new ArrayList<>();
        icon.add(R.drawable.star1);
        icon.add(R.drawable.star1);
        icon.add(R.drawable.star1);
        viewPager.setAdapter(new SliderAdapter(this, icon));
        indicator.setupWithViewPager(viewPager, true);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);

        findViewById(R.id.nav_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(navigationView);
            }
        });
        findViewById(R.id.cart_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,CartActivity.class);
                startActivity(i);
            }
        });
    }

    private void getProductCategory() {
        fullScreenLoader.show();
        final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        retrofit2.Call<ProductCategory> call = apiService.getProductCategory();
        call.enqueue(new Callback<ProductCategory>() {
            @Override
            public void onResponse(Call<ProductCategory> call, Response<ProductCategory> response) {
                ProductCategory productCategory = response.body();
                if (response.code() == 200) {
                    fullScreenLoader.dismiss();
                    viewPager.setVisibility(View.VISIBLE);
                    productCategoryModelArrayList = (ArrayList<Categories>) productCategory.getCategory();
                    productCategoryAdapter = new ProductCategoryAdapter(getApplicationContext(), productCategoryModelArrayList);
                    productCategoryRecyclerView.setAdapter(productCategoryAdapter);
                    // Toast.makeText(MainActivity.this, new Gson().toJson(productCategory), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "OOPS! Something wrong happened", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProductCategory> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                fullScreenLoader.dismiss();
                Log.d("Error", t.toString());
            }
        });
    }

    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() < icon.size() - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }
}