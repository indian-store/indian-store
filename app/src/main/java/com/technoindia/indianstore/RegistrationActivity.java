package com.technoindia.indianstore;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.technoindia.indianstore.Api.ApiClient;
import com.technoindia.indianstore.Api.ApiInterface;
import com.technoindia.indianstore.Model.LoginResponse;
import com.technoindia.indianstore.Model.RegistrationResponse;
import com.technoindia.indianstore.Utils.FullScreenLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class RegistrationActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    TextInputEditText edt_name, edt_phone, edt_email, edt_password;
    TextInputLayout txt_input_layout_name, txt_input_layout_phone, txtInputLayoutEmail, txtInputLayoutPassword;
    String storeName = "", storePhone = "", storeEmail = "", storePassword = "";
    Button btn_register;
    FullScreenLoader fullScreenLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        fullScreenLoader = new FullScreenLoader(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        edt_name = findViewById(R.id.edt_name);
        edt_phone = findViewById(R.id.edt_phone);
        edt_email = findViewById(R.id.edt_email);
        edt_password = findViewById(R.id.edt_password);
        txt_input_layout_name = findViewById(R.id.txt_input_layout_name);
        txt_input_layout_phone = findViewById(R.id.txt_input_layout_phone);
        txtInputLayoutEmail = findViewById(R.id.txt_input_layout_email);
        txtInputLayoutPassword = findViewById(R.id.txt_input_layout_password);
        btn_register = findViewById(R.id.btn_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    onRegistration();
                }
            }
        });
    }

    private void onRegistration() {
        fullScreenLoader.show();
        final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        JsonObject jsonObject = new JsonObject();
        JsonObject mainObject = new JsonObject();
        jsonObject.addProperty("jsonrpc", "2.0");

        mainObject.addProperty("name", storeName);
        mainObject.addProperty("phone", storePhone);
        mainObject.addProperty("password", storePassword);
        mainObject.addProperty("email", storeEmail);

        jsonObject.add("params", mainObject);


        retrofit2.Call<RegistrationResponse> call = apiService.registration(jsonObject);
        call.enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, retrofit2.Response<RegistrationResponse> response) {
                RegistrationResponse registrationResponse = response.body();
                if (response.code() == 200 && registrationResponse != null) {
                    fullScreenLoader.dismiss();
                    if (registrationResponse.getSuccess() != null) {
                        if (registrationResponse.getSuccess().getMessage().equalsIgnoreCase("Success!")) {
                            Toast.makeText(RegistrationActivity.this, registrationResponse.getSuccess().getMeaning(), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(RegistrationActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                fullScreenLoader.dismiss();
                Toast.makeText(RegistrationActivity.this, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean isValidate() {
        storeName = Objects.requireNonNull(edt_name.getText()).toString().trim();
        storePhone = Objects.requireNonNull(edt_phone.getText()).toString().trim();
        storeEmail = Objects.requireNonNull(edt_email.getText()).toString().trim();
        storePassword = Objects.requireNonNull(edt_password.getText()).toString().trim();

        /** name validation **/
        if (storeName.isEmpty()) {
            Toast.makeText(this, "Enter your Name",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else {

        }

        /** phone validation **/
        if (storePhone.isEmpty()) {
            Toast.makeText(this, "Enter your Phone",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (storePhone.length() < 10) {
                Toast.makeText(this, "Enter 10 Digits mobile number",
                        Toast.LENGTH_SHORT).show();
                return false;
            } else {

            }
        }

        /** email validation **/
        if (storeEmail.isEmpty()) {
            Toast.makeText(this, "Enter your email",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else {
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (storeEmail.matches(emailPattern) && storeEmail.length() > 0) {

            } else {
                Toast.makeText(this, "Invalid email address",
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        /** password validation **/
        if (storePassword.isEmpty()) {
            Toast.makeText(this, "Enter your password",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (storePassword.length() >= 6) {

            } else {
                Toast.makeText(this, "Please enter minimum six digit password",
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }
}