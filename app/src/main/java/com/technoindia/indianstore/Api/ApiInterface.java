package com.technoindia.indianstore.Api;
import com.google.gson.JsonObject;
import com.technoindia.indianstore.Model.FashionCategoryResponse;
import com.technoindia.indianstore.Model.LoginResponse;
import com.technoindia.indianstore.Model.ProductCategory;
import com.technoindia.indianstore.Model.ProductDetailsResponse;
import com.technoindia.indianstore.Model.RegistrationResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @POST("login")
    Call<LoginResponse> login(@Body JsonObject locationPost);

    @GET("home")
    Call<ProductCategory> getProductCategory();

    @GET("product-list")
    Call<FashionCategoryResponse> getFashionCategory(@Query("category_id") int category_id);

    @GET("product-details")
    Call<ProductDetailsResponse> getProjectDetails(@Query("product_id") int product_id);

    @POST("register")
    Call<RegistrationResponse> registration(@Body JsonObject registration);
}