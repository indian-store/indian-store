package com.technoindia.indianstore.Model;

public class Categories {
    private int id;
    private float parent_id;
    private String name;
    private String slug;
    private float display_order;
    private String image;
    private String status;
    private String created_at;
    private String updated_at;


    // Getter Methods

    public int getId() {
        return id;
    }

    public float getParent_id() {
        return parent_id;
    }

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public float getDisplay_order() {
        return display_order;
    }

    public String getImage() {
        return image;
    }

    public String getStatus() {
        return status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    // Setter Methods

    public void setId(int id) {
        this.id = id;
    }

    public void setParent_id(float parent_id) {
        this.parent_id = parent_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setDisplay_order(float display_order) {
        this.display_order = display_order;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
