package com.technoindia.indianstore.Model;

import java.util.List;

public class FashionCategoryResponse {
    private List<FashionProductCategory> product = null;

    /*@SerializedName("get_category")
    @Expose
    private GetCategory getCategory;*/

    public List<FashionProductCategory> getProduct() {
        return product;
    }

    public void setProduct(List<FashionProductCategory> product) {
        this.product = product;
    }

    /*public GetCategory getGetCategory() {
        return getCategory;
    }

    public void setGetCategory(GetCategory getCategory) {
        this.getCategory = getCategory;
    }*/
}
