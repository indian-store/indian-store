package com.technoindia.indianstore;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CartActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Button removeCart;
    ImageView productImage;
    TextView productName, productAfterDiscount;
    Spinner productQuantity;
    ArrayList<String> spinnerArray;
    ArrayList<String> mPreviousData;
    RelativeLayout emptyCartLayout;
    LinearLayout productDetailsLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        productName = findViewById(R.id.product_name);
        productAfterDiscount = findViewById(R.id.product_after_discount);
        productImage = findViewById(R.id.product_image);
        removeCart = findViewById(R.id.remove_cart);
        productQuantity = findViewById(R.id.product_quantity);
        productQuantity.setOnItemSelectedListener(this);
        emptyCartLayout = findViewById(R.id.empty_cart_layout);
        productDetailsLayout = findViewById(R.id.product_details_layout);

        if (getIntent().hasExtra("Data")) {
            mPreviousData = getIntent().getStringArrayListExtra("Data");
            productName.setText(mPreviousData.get(0));
            productAfterDiscount.setText(mPreviousData.get(3));
            Picasso.with(CartActivity.this)
                    .load(mPreviousData.get(2))
                    .into(productImage);
        }

        removeCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(CartActivity.this);
                builder1.setMessage("Want to Remove item ?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Are you sure ?",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // dialog.cancel();
                                emptyCartLayout.setVisibility(View.VISIBLE);
                                productDetailsLayout.setVisibility(View.GONE);
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        spinnerArray = new ArrayList<String>();
        spinnerArray.add("1");
        spinnerArray.add("2");
        spinnerArray.add("3");
        spinnerArray.add("4");
        spinnerArray.add("5");


        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerArray);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        productQuantity.setAdapter(aa);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (spinnerArray.get(position).equalsIgnoreCase("1")) {
            productAfterDiscount.setText(mPreviousData.get(3));
        } else if (spinnerArray.get(position).equalsIgnoreCase("2")) {
            int productPrice = Integer.parseInt(mPreviousData.get(3)) * 2;
            productAfterDiscount.setText(String.valueOf(productPrice));
        }
        else if (spinnerArray.get(position).equalsIgnoreCase("3")) {
            int productPrice = Integer.parseInt(mPreviousData.get(3)) * 3;
            productAfterDiscount.setText(String.valueOf(productPrice));
        }
        else if (spinnerArray.get(position).equalsIgnoreCase("4")) {
            int productPrice = Integer.parseInt(mPreviousData.get(3)) * 4;
            productAfterDiscount.setText(String.valueOf(productPrice));
        }
        else if (spinnerArray.get(position).equalsIgnoreCase("5")) {
            int productPrice = Integer.parseInt(mPreviousData.get(3)) * 5;
            productAfterDiscount.setText(String.valueOf(productPrice));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}