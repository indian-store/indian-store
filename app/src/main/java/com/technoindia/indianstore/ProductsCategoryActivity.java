package com.technoindia.indianstore;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.technoindia.indianstore.Adapter.ProductsAdapter;
import com.technoindia.indianstore.Api.ApiClient;
import com.technoindia.indianstore.Api.ApiInterface;
import com.technoindia.indianstore.Model.FashionCategoryResponse;
import com.technoindia.indianstore.Model.FashionProductCategory;
import com.technoindia.indianstore.Utils.FullScreenLoader;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsCategoryActivity extends AppCompatActivity {
    int categoryId = 0;
    String saveCategoryName= "";
    FullScreenLoader fullScreenLoader;
    RecyclerView fashionRecyclerView;
    ArrayList<FashionProductCategory> fashionCategoryModelArrayList;
    ProductsAdapter fashionProductAdapter;
    ImageView backButton;
    TextView categoryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_category);
        fashionRecyclerView = findViewById(R.id.fashion_recycler_view);
        backButton = findViewById(R.id.img_btn_back);
        categoryName = findViewById(R.id.category_name);
        fullScreenLoader = new FullScreenLoader(this);
        if (getIntent().hasExtra("category_id")) {
            categoryId = getIntent().getIntExtra("category_id", 0);
            // Toast.makeText(this, categoryId + "", Toast.LENGTH_SHORT).show();
        }
        if (getIntent().hasExtra("category_name")) {
            saveCategoryName = getIntent().getStringExtra("category_name");
            categoryName.setText(saveCategoryName);
            // Toast.makeText(this, categoryId + "", Toast.LENGTH_SHORT).show();
        }
        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        fashionRecyclerView.setLayoutManager(manager);
        getProductCategory();
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getProductCategory() {
        fullScreenLoader.show();
        final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        retrofit2.Call<FashionCategoryResponse> call = apiService.getFashionCategory(categoryId);
        call.enqueue(new Callback<FashionCategoryResponse>() {
            @Override
            public void onResponse(Call<FashionCategoryResponse> call, Response<FashionCategoryResponse> response) {
                FashionCategoryResponse fashionCategoryResponse = response.body();
                if (response.code() == 200 && fashionCategoryResponse != null) {
                    fullScreenLoader.dismiss();
                    fashionCategoryModelArrayList = (ArrayList<FashionProductCategory>) fashionCategoryResponse.getProduct();
                    fashionProductAdapter = new ProductsAdapter(getApplicationContext(), fashionCategoryModelArrayList);
                    fashionRecyclerView.setAdapter(fashionProductAdapter);
                    // Toast.makeText(MainActivity.this, new Gson().toJson(productCategory), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "OOPS! Something wrong happened", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<FashionCategoryResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                fullScreenLoader.dismiss();
                Log.d("Error", t.toString());
            }
        });
    }
}