package com.technoindia.indianstore.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
;
import com.technoindia.indianstore.Model.FashionProductCategory;
import com.technoindia.indianstore.ProductDetailsActivity;
import com.technoindia.indianstore.R;

import java.util.ArrayList;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.viewHolder> {

    Context context;
    ArrayList<FashionProductCategory> arrayList;

    public ProductsAdapter(Context context, ArrayList<FashionProductCategory> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.fashion_category_list_item, viewGroup, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(viewHolder viewHolder, int position) {
        viewHolder.productName.setText(arrayList.get(position).getTitle());
        viewHolder.productCategory.setText(arrayList.get(position).getGetCategory().getName());
        viewHolder.productPrice.setText("₹" + " " + arrayList.get(position).getPrice());
        viewHolder.productDiscountPrice.setPaintFlags(viewHolder.productDiscountPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        viewHolder.productDiscountPrice.setText("₹" + " " + arrayList.get(position).getDiscount());
        viewHolder.productDiscountAmount.setText(String.valueOf(arrayList.get(position).getDiscount())+ "%" + " " + "off");
        Picasso.with(context)
                .load("https://xxotik.io/xxotik/indianstore/storage/app/product/" + arrayList.get(position).getImage())
                .into(viewHolder.productImage);
        viewHolder.productDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent i = new Intent(context, ProductDetailsActivity.class);
                    i.putExtra("product_id", arrayList.get(position).getId());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {
        ImageView productImage;
        TextView productName, productCategory, productPrice,
                productDiscountPrice, productDiscountAmount;
        LinearLayout productDetails;

        public viewHolder(View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.product_image);
            productName = itemView.findViewById(R.id.product_name);
            productCategory = itemView.findViewById(R.id.product_category);
            productPrice = itemView.findViewById(R.id.product_price);
            productDiscountPrice = itemView.findViewById(R.id.product_discount_price);
            productDiscountAmount = itemView.findViewById(R.id.product_discount_amount);
            productDetails = itemView.findViewById(R.id.product_details);
        }
    }
}