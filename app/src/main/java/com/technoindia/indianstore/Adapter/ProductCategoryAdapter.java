package com.technoindia.indianstore.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.technoindia.indianstore.ProductsCategoryActivity;
import com.technoindia.indianstore.Model.Categories;
import com.technoindia.indianstore.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductCategoryAdapter extends RecyclerView.Adapter<ProductCategoryAdapter.viewHolder> {

    Context context;
    ArrayList<Categories> arrayList;

    public ProductCategoryAdapter(Context context, ArrayList<Categories> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_category_list_item, viewGroup, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(viewHolder viewHolder, int position) {
        viewHolder.productName.setText(arrayList.get(position).getName());
        Picasso.with(context)
                .load("https://xxotik.io/xxotik/indianstore/storage/app/category/" + arrayList.get(position).getImage())
                .into(viewHolder.productImage);
        viewHolder.productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ProductsCategoryActivity.class);
                i.putExtra("category_id",arrayList.get(position).getId());
                i.putExtra("category_name",arrayList.get(position).getName());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {
        ImageView productImage;
        TextView productName;

        public viewHolder(View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.product_image);
            productName = itemView.findViewById(R.id.product_name);
        }
    }
}
