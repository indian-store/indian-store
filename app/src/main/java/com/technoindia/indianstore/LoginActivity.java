package com.technoindia.indianstore;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.technoindia.indianstore.Api.ApiClient;
import com.technoindia.indianstore.Api.ApiInterface;
import com.technoindia.indianstore.Model.LoginErrorResponse;
import com.technoindia.indianstore.Model.LoginResponse;
import com.technoindia.indianstore.Utils.FullScreenLoader;
import com.technoindia.indianstore.Utils.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    TextInputEditText edt_email, edt_password;
    TextInputLayout txtInputLayoutEmail, txtInputLayoutPassword;
    TextView register;
    String storeEmail = "", storePassword = "";
    Button btn_login;
    FullScreenLoader fullScreenLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        fullScreenLoader = new FullScreenLoader(this);
        register = findViewById(R.id.register_now);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        edt_email = findViewById(R.id.edt_email);
        edt_password = findViewById(R.id.edt_password);
        txtInputLayoutEmail = findViewById(R.id.txt_input_layout_email);
        txtInputLayoutPassword = findViewById(R.id.txt_input_layout_password);
        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidate()) {
                    onLogin();
                }
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                startActivity(i);
                // finish();
            }
        });
    }

    private void onLogin() {
        fullScreenLoader.show();
        final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        JsonObject jsonObject = new JsonObject();
        JsonObject mainObject = new JsonObject();
        jsonObject.addProperty("jsonrpc", "2.0");

        mainObject.addProperty("password", storePassword);
        mainObject.addProperty("email", storeEmail);

        jsonObject.add("params", mainObject);


        retrofit2.Call<LoginResponse> call = apiService.login(jsonObject);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();
                if (response.code() == 200 && loginResponse != null) {
                    fullScreenLoader.dismiss();
                    SharedPrefManager prefManager = new SharedPrefManager(LoginActivity.this);
                    prefManager.saveUserDetails();
                    if (loginResponse.getResult().getStatus() != null) {
                        if (loginResponse.getResult().getStatus().equalsIgnoreCase("A")) {
                            String saveToken = loginResponse.getResult().getToken();
                            Toast.makeText(LoginActivity.this, "Login Successful" + saveToken, Toast.LENGTH_LONG).show();
                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            startActivity(i);
                            finish();
                        } else {
                           /* LoginErrorResponse loginErrorResponse = response.errorBody();
                            if (loginErrorResponse.getError().equals("Error!")){

                            }*/
                            JSONObject jObjError = null;
                            try {
                                jObjError = new JSONObject(response.errorBody().string());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            try {
                                Toast.makeText(LoginActivity.this, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            // Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                fullScreenLoader.dismiss();
                // Toast.makeText(LoginActivity.this, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean isValidate() {
        storeEmail = Objects.requireNonNull(edt_email.getText()).toString().trim();
        storePassword = Objects.requireNonNull(edt_password.getText()).toString().trim();

        /** email validation **/
        if (storeEmail.isEmpty()) {
            Toast.makeText(this, "Enter your email",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else {
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (storeEmail.matches(emailPattern) && storeEmail.length() > 0) {

            } else {
                Toast.makeText(this, "Invalid email address",
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        /** password validation **/
        if (storePassword.isEmpty()) {
            Toast.makeText(this, "Enter your password",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (storePassword.length() >= 5) {

            } else {
                Toast.makeText(this, "Please enter minimum five digit password",
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }
}