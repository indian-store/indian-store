package com.technoindia.indianstore.Utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import com.technoindia.indianstore.R;


public class FullScreenLoader {

    private static Dialog dialog;
    Context context;

    public FullScreenLoader(Context context) {
        this.context = context;
        Loader();
    }

    public void show() {
        // dialog.show();
        try {
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            // use a log message
        }
    }

    private void Loader() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.full_screen_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.setCancelable(false);
    }

    public void dismiss() {
        dialog.dismiss();
    }

}
