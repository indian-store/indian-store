package com.technoindia.indianstore.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SharedPrefManager {
    private static final String SHARED_PREF_NAME = "indian_store_shared_pref";
    public static final String TOKEN = "token";
    public static final String USER_ID = "user_id";
    public static final String TENANT_ID = "tenant_id";
    public static final String USER_NAME = "user_name";
    public static final String PHONE_NO = "phone_no";
    public static final String IS_LOGIN = "isLogIn";
    public static final String PHOTO_URL = "photo_url";


    Context mContext;
    SharedPreferences prefManager;
    SharedPreferences.Editor editor;

    public SharedPrefManager(Context mContext) {
        this.mContext = mContext;
        prefManager = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        editor = prefManager.edit();
    }

    /*public void saveUserDetails(String token, String userId, String tenantId, String userName, String phoneNumber, String userImg) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(TOKEN, token);
        editor.putString(USER_ID, userId);
        editor.putString(TENANT_ID, tenantId);
        editor.putString(USER_NAME, userName);
        editor.putString(PHONE_NO, phoneNumber);
        editor.putString(PHOTO_URL, userImg);
        editor.commit();
    }*/

    public void saveUserDetails() {
        editor.putBoolean(IS_LOGIN, true);
        editor.commit();
    }

    public boolean checkLogIn() {
        boolean loginStatus = prefManager.getBoolean(IS_LOGIN, false);
        return loginStatus;
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> userDetails = new HashMap<String, String>();
        userDetails.put(TOKEN, prefManager.getString(TOKEN, null));
        userDetails.put(USER_ID, prefManager.getString(USER_ID, null));
        userDetails.put(TENANT_ID, prefManager.getString(TENANT_ID, null));
        userDetails.put(USER_NAME, prefManager.getString(USER_NAME, null));
        userDetails.put(PHONE_NO, prefManager.getString(PHONE_NO, null));
        userDetails.put(PHOTO_URL, prefManager.getString(PHOTO_URL, null));
        return userDetails;
    }

    public void clear() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}

