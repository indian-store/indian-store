package com.technoindia.indianstore;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.technoindia.indianstore.Api.ApiClient;
import com.technoindia.indianstore.Api.ApiInterface;
import com.technoindia.indianstore.Model.ProductDetailsResponse;
import com.technoindia.indianstore.Utils.FullScreenLoader;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class ProductDetailsActivity extends AppCompatActivity {
    FullScreenLoader fullScreenLoader;
    int productId = 0;
    TextView productNameHeader, productName, productCategory, productPrice, productDiscountPrice, productDiscountAmount;
    ImageView productImage;
    Button addToCart;
    String saveProductName = "", saveProductCategory = "", saveProductImage = "";
    Integer saveProductPrice = 0, saveProductPriceDiscount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        productNameHeader = findViewById(R.id.product_name_header);
        productImage = findViewById(R.id.product_image);
        productName = findViewById(R.id.product_name);
        productCategory = findViewById(R.id.product_category);
        productPrice = findViewById(R.id.product_price);
        productDiscountPrice = findViewById(R.id.product_discount_price);
        productDiscountAmount = findViewById(R.id.product_off);
        addToCart = findViewById(R.id.add_to_cart);

        productPrice.setPaintFlags(productDiscountPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        // productDiscountAmount = findViewById(R.id.product_discount_amount);
        fullScreenLoader = new FullScreenLoader(this);
        if (getIntent().hasExtra("product_id")) {
            productId = getIntent().getIntExtra("product_id", 0);
            // Toast.makeText(this, categoryId + "", Toast.LENGTH_SHORT).show();
        }
        getProductDetails();
        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> mData = new ArrayList<>();
                mData.add(saveProductName);//0
                mData.add(saveProductCategory);//1
                mData.add(saveProductImage);//2
                mData.add(String.valueOf(saveProductPrice));//3
                mData.add(String.valueOf(saveProductPriceDiscount));//4
                Intent i = new Intent(ProductDetailsActivity.this, CartActivity.class);
                i.putExtra("Data", mData);
                startActivity(i);
                /*Intent i = new Intent(ProductDetailsActivity.this, CartActivity.class);
                i.putExtra("product_name",saveProductName);
                i.putExtra("product_category",saveProductCategory);
                i.putExtra("product_image",saveProductImage);
                i.putExtra("product_price",saveProductPrice);
                i.putExtra("product_discount",saveProductPriceDiscount);
                startActivity(i);*/
            }
        });
    }

    private void getProductDetails() {
        fullScreenLoader.show();
        final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        retrofit2.Call<ProductDetailsResponse> call = apiService.getProjectDetails(productId);
        call.enqueue(new Callback<ProductDetailsResponse>() {
            @Override
            public void onResponse(Call<ProductDetailsResponse> call, retrofit2.Response<ProductDetailsResponse> response) {
                ProductDetailsResponse productDetailsResponse = response.body();
                if (response.code() == 200 && productDetailsResponse != null) {
                    fullScreenLoader.dismiss();
                    if (productDetailsResponse.getProduct() != null) {
                        if (productDetailsResponse.getProduct().get(0).getTitle() != null) {
                            productNameHeader.setText(productDetailsResponse.getProduct().get(0).getTitle());
                        }
                        if (productDetailsResponse.getProduct().get(0).getImage() != null) {
                            saveProductImage = "https://xxotik.io/xxotik/indianstore/storage/app/product/" + productDetailsResponse.getProduct().get(0).getImage();
                            Picasso.with(ProductDetailsActivity.this)
                                    .load("https://xxotik.io/xxotik/indianstore/storage/app/product/" + productDetailsResponse.getProduct().get(0).getImage())
                                    .into(productImage);
                        }
                        if (productDetailsResponse.getProduct().get(0).getTitle() != null) {
                            productName.setText(productDetailsResponse.getProduct().get(0).getTitle());
                            saveProductName = productDetailsResponse.getProduct().get(0).getTitle();
                        }
                        if (productDetailsResponse.getProduct().get(0).getGetCategory().getName() != null) {
                            productCategory.setText(productDetailsResponse.getProduct().get(0).getGetCategory().getName());
                            saveProductCategory = productDetailsResponse.getProduct().get(0).getGetCategory().getName();
                        }
                        if (productDetailsResponse.getProduct().get(0).getPrice() != null) {
                            productPrice.setText("₹" + " " + productDetailsResponse.getProduct().get(0).getPrice());
                            saveProductPrice = productDetailsResponse.getProduct().get(0).getPrice();
                        }
                        int afterDiscountPrice = (productDetailsResponse.getProduct().get(0).getPrice() - (productDetailsResponse.getProduct().get(0).getPrice() * (productDetailsResponse.getProduct().get(0).getDiscount() / 100)));
                        // Toast.makeText(ProductDetailsActivity.this, afterDiscountPrice + "", Toast.LENGTH_SHORT).show();
                        // Log.i("res", String.valueOf(productDetailsResponse.getProduct().get(0)));
                        Log.i("res", new Gson().toJson(productDetailsResponse.getProduct().get(0)));
                        productDiscountPrice.setText("₹" + " " + afterDiscountPrice);
                        if (productDetailsResponse.getProduct().get(0).getDiscount() != null) {
                            productDiscountAmount.setText(productDetailsResponse.getProduct().get(0).getDiscount() + "%" + " " + "off");
                            saveProductPriceDiscount = productDetailsResponse.getProduct().get(0).getDiscount();
                        }
                        // Toast.makeText(ProductDetailsActivity.this, new Gson().toJson(productDetailsResponse), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductDetailsResponse> call, Throwable t) {
                fullScreenLoader.dismiss();
                Toast.makeText(ProductDetailsActivity.this, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
